package Tutorial4;
import java.util.ArrayList;

public class BankAccountAppArrayList {
	public static void main (String[] args) {
		ArrayList<BankAccount> list = new ArrayList<BankAccount>();
		
		list.add(new BankAccount(1000));
		list.add(new BankAccount(2000));
		list.add(new BankAccount(3000));
		list.add(new BankAccount(4000));
		list.add(new BankAccount(5000));
		
		double totalAmt = 0;
		for (BankAccount acc : list) {
			totalAmt += acc.getBalance();
		}
		
		System.out.printf("Total balance: $%.2f\n", totalAmt);
	}
}
