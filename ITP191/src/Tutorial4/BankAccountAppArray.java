package Tutorial4;

public class BankAccountAppArray {
	static final int SIZE = 5;
	public static void main (String[] args) {
		
		BankAccount [] list = new BankAccount[SIZE];
		
		list[0] = new BankAccount(1000);
		list[1] = new BankAccount(2000);
		list[2] = new BankAccount(3000);
		list[3] = new BankAccount(4000);
		list[4] = new BankAccount(5000);
		
		double totalAmt = 0;
		for (BankAccount acc : list) {
			totalAmt += acc.getBalance();
		}
		
		System.out.printf("Total balance: $%.2f\n", totalAmt);
	}
}
