package Tutorial4;
import java.util.ArrayList;
import java.util.Scanner;

public class SupermarketHallOfFame {
	public static String getBestCustomer(ArrayList<Double> saleList, ArrayList<String> custList) {
		double max = saleList.get(0);
		String top = " ";
		
		for (int i = 0 ; i < saleList.size(); i++) {
			if (saleList.get(i) > max) {
				max = saleList.get(i);
				top = custList.get(i);
			}
		}
		return top;
	}
	
	public static void main (String[] args) {
		Scanner in = new Scanner (System.in);
		
		ArrayList<Double> price = new ArrayList<Double>();
		ArrayList<String> name = new ArrayList<String>();
		
		boolean done = false;
		
		while (!done) {
			System.out.print("Enter the price: ");
			double d = in.nextDouble();
			if (d == 0) {
				done = true;
			}
			else {
				price.add(d);
				System.out.print("Customer Name: ");
				name.add(in.next());
			}
		}
		System.out.print("Best Customer's Name: " + getBestCustomer(price, name));
		in.close();
	}
}
