package Practical1;
import java.util.Arrays;
import java.util.Scanner;

public class Question3Answers {
	public static void main (String[] args) {
		
		String grade = getGrade();
		double numericvalue = convertGrade(grade);
		System.out.print("The numeric value is " + numericvalue + ".");
	}
	
	public static String getGrade() {
		Scanner in = new Scanner (System.in);
		String tempGrade, grade;
		boolean valid;
		
		String[] validGrades = new String[] {"A", "B", "C", "D", "F"};
		
		do {
			valid = true;
			System.out.print("Enter letter grade: ");
			
			tempGrade = in.next();
			grade = tempGrade.toUpperCase();
			
			if (grade.length() != 1)
				valid = false;
			else {
				if (!Arrays.asList(validGrades).contains(grade)) {
					valid = false;
					System.out.println("Invalid grade.");
				}
			}
		} while (!valid);
		return grade;
	}
	
	public static double convertGrade (String grade) {
		double numericvalue = 0;
		
		final double A = 4.0;
		final double B = 3.0;
		final double C = 2.0;
		final double D = 1.0;
		final double F = 0;
		
		switch (grade.charAt(0)) {
		
		case 'A': {
			numericvalue = A;
		}
		case 'B': {
			numericvalue = B;
		}
		case 'C': {
			numericvalue = C;
		}
		case 'D': {
			numericvalue = D;
		}
		case 'F': {
			numericvalue = F;
		}
		}
		
		return numericvalue;
	}
}
