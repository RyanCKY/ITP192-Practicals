package Practical1;
import java.util.Scanner;

public class Question3 {
	public static void main (String[] args) {
		Scanner in = new Scanner (System.in);
		
		String grade;
		int value = 0;
		
		System.out.print("Enter letter grade: ");
		grade = in.nextLine();
		
		if (grade.equalsIgnoreCase("A")) {
			value = 4;
		}
		else if (grade.equalsIgnoreCase("B")) {
			value = 3;
		}
		else if (grade.equalsIgnoreCase("C")) {
			value = 2;
		}
		else if (grade.equalsIgnoreCase("D")) {
			value = 1;
		}
		else if (grade.equalsIgnoreCase("F")) {
			value = 0;
		}
		
		System.out.print("The numeric value is " + value + ".");
	}
}
