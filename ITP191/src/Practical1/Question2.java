package Practical1;
/**
	  Prompts a user to enter a value in a given range until the user
	  provides a valid input.
	  @param low the low end of the range
	  @param high the high end of the range
	  @return the value provided by the user
	*/

import java.util.Scanner;

public class Question2 {
	
	public static int readValueBetween(int low, int high) {
		int input;
		Scanner in = new Scanner(System.in);
		do {
			System.out.print("Enter a value between " + low + " and " + high + ": ");
			input = in.nextInt();
		} while (input < low || input > high);
		return input;
	}
	
	public static double readDouble(String prompt) {
		System.out.println(prompt + " ");
		Scanner in = new Scanner(System.in);
		return in.nextDouble();
	}
	
	// Can add readString
	// Can add readDoubleBetween
	
	public static double readDoubleBetween (int low, int high) {
		int input;
		Scanner in = new Scanner (System.in);
		do {
			System.out.print("Enter a decimal between " + low + " and " + high + ": ");
			input = in.nextInt();
		} while (input < low || input > high);
		return input;
	}
	
	// Can add readDoubleGreaterThan
	
	public static int readDoubleGreaterThan (int low, int high) {
		int input, greater;
		String message;
		Scanner in = new Scanner (System.in);
		
		message = "Input not high enough.";
		
		do {
			System.out.print("Enter a value between " + low + " and " + high + ": ");
			input = in.nextInt();
		} while (input < low || input > high);
		if (input > 10) {
			return input;
		}
		return input;
		}
	// Can add readValueGreaterThan
	// etc...

	public static void main(String[] args) {
		int hours = Question2.readValueBetween(1, 12);
		int minutes= Question2.readValueBetween(0, 59);
		// Output
		System.out.println(hours + ":" + minutes);
	}
}
