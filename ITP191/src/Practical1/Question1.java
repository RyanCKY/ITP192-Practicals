package Practical1;
import java.util.Scanner;

public class Question1 {
	public static void main (String args[]) {
		
		double salary = readDouble("Please enter your salary: $");
		double payRise = readDouble("Please enter salary increment in %: ");
		System.out.println("Salary entered: " + salary);
		System.out.print("Salary increment: " + payRise);
	}
	
	public static double readDouble (String prompt) {
		System.out.print(prompt);
		Scanner in = new Scanner (System.in);
		return in.nextDouble();
	}
}
