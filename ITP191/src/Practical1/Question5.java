package Practical1;
import java.util.Scanner;

public class Question5 {
	public static void main (String[] args) {
		Scanner in = new Scanner (System.in);
		
		double bank, intrest, expense, estimate, intrestvalue, a, total = 0;
		
		System.out.print("Enter starting account balance: $");
		bank = in.nextDouble();
		
		System.out.print("Enter compound intrest per month (%): ");
		intrest = in.nextDouble();
		
		System.out.print("Enter expense per month: $");
		expense = in.nextDouble();
		
		do {
			intrestvalue = (bank*intrest)/100;
			a = expense - intrestvalue;
			estimate = bank/a;
		} while (total > expense);
		
		System.out.print("The account will be completely depleted in " + estimate + " months");
		
		in.close();
	}
}