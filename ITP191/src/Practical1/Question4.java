package Practical1;
import java.util.Scanner;

public class Question4 {
	
	public static int getMaxValue(int[] array){  
	      int maxValue = array[0];  
	      for(int i=1;i < array.length;i++){  
	      if(array[i] > maxValue){  
	      maxValue = array[i];  

	         }  
	     }  
	             return maxValue;  
	}  
	
	public static void main (String[] args) {
		Scanner in = new Scanner (System.in);
		
		String [] month = {"January", "Febuary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
		int [] tempArr = new int[12];
		int highest = 1;
		double high = tempArr[highest];		
		
		for (int i = 0 ; i < 12 ; i++) {
			System.out.print("Enter temperature for the month of " + month[i] + " -> ");
			tempArr[i] = in.nextInt();
		}
		
		for (int i = 0; i < tempArr.length ; i++) {
			if (tempArr[i] > high) {
				high = tempArr[i];
				highest = i;
			}
		}
		
		System.out.println(" ");
		System.out.println("Highest temperature is " + high);
		System.out.println("The month with the highest temperature is " + highest);
		System.out.print("The month with the highest temperature is " + month[highest]);
	}
}
