package Practical2;

public class Student {
	// Attributes of Student
	private String name;
	private char gender;
	private double mark;

	// Constructor
	public Student(String name, char gender) {
		this.name = name;
		this.gender = gender;
	}

	// Accessor & Mutator methods
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}

	public double getMark() {
		return mark;
	}

	public void setMark(double mark) {
		this.mark = mark;
	}
	// User defined or service method
	public void view() {
		System.out.println("Name: " + name);
		System.out.println("Gender: " + gender);
		System.out.println("Marks: " + mark);
	}
}
