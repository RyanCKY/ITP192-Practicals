package Tutorial3;
import java.util.Scanner;

public class BankAccountApp {
	public static void main (String[] args) {
		Scanner in = new Scanner (System.in);
		
		int accountno;
		double initialbal, afterbal = 0, deposit, withdraw;
		
		System.out.println("This program will show your balance after fees, deposits and withdrawals.");
		System.out.println("=========================================================================");
		
		System.out.print("Enter initial balance: $");
		initialbal = in.nextDouble();
		
		System.out.print("Enter deposits: $");
		deposit = in.nextDouble();
		
		System.out.print("Enter withdrawals: $");
		withdraw = in.nextDouble();
		
		BankAccount new1 = new BankAccount(initialbal);
		new1.deposit(deposit);
		boolean withdrawStatus = false;
		withdrawStatus = new1.withdraw(withdraw);
		
		if(!withdrawStatus)
			System.out.println(" ");
			System.out.println("Error performing withdrawal, withdrawl is not done.");
		
		afterbal = new1.getBalance();
		accountno = new1.getAccountNo();
		
		System.out.println(" ");
		
		System.out.println("Your account number is " + accountno);
		System.out.println("The Bank Overdraft fee is $" + new1.getOverdraft());
		System.out.print("Your final balance is $" + afterbal);
		
		in.close();
	}
}
