package Tutorial3;

public class BankAccount {
	private int accountno;
	private double balance;
	static double overdraft = 30.00;
	private static int lastAssignedNumber = 1000;
	
	public BankAccount() {
		lastAssignedNumber++;
		accountno = lastAssignedNumber;
	}
	
	public BankAccount (double balance) {
		this();
		this.balance = balance;
	}
	
	public int getAccountNo() {
		return accountno;
	}
	
	public double getBalance () {
		balance = balance - overdraft;
		return balance;
	}
	
	public void setAccountNo (int accountno) {
		this.accountno = accountno;
	}
	
	public void setBalance (double balance) {
		this.balance = balance;
	}
	
	public double getOverdraft () {
		if (balance >= 0) {
			overdraft = 0;
		}
		return overdraft;
	}
	
	public boolean deposit (double amt) {
		if (amt > 0) {
			balance += amt;
		}
		else
			return false;
		return true;
	}
	
	public boolean withdraw (double amt) {
		if (amt <= balance) {
			balance -= amt;
		}
		else
			return false;
		return true;
	}
}
