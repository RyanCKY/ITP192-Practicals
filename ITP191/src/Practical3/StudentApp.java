package Practical3;
import java.util.Scanner;

public class StudentApp {
	public static void main (String[] args) {
		Scanner in = new Scanner (System.in);
		String name;
		int quiz = 0;
		
		System.out.print("Enter name: ");
		name = in.nextLine();
		
		Student s = new Student(name);
		
		for (int i = 1 ; i <= 5 ; i++) {
			System.out.print("Enter score for test " + i + " :");
			quiz = in.nextInt();
			s.addQuiz(quiz);
		}
		
		System.out.print("\n");
		
		s.getAverageScore();
		s.view();
		
		in.close();
	}
}
