package Practical3;

public class Student {
	private String name;
	private int quiz;
	private int score;
	private double average;

	public Student() {
		
	}

	public Student(String name) {
		this.name = name;
	}
	
	public void addQuiz (int score) {
		this.score += score;
		quiz++;
	}
	
	public int getTotalScore() {
		return score;
	}
	
	public double getAverageScore() {
		average = (double)score/quiz;
		return average;
	}
	
	public void view() {
		System.out.println("Name: " + name);
		System.out.println("Numer of quizes taken: " + quiz);
		System.out.println("Total Score: " + score);
		System.out.println("Average score: " + average);
	}
}