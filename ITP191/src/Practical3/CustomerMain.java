package Practical3;

//The program will not compile. Because the variable it is trying to access is private.

public class CustomerMain {
	public static void main (String[] args) {
		Customer cust = new Customer("Ms Phoon", 123);
		System.out.println("Name: " + cust.name);
	}
}