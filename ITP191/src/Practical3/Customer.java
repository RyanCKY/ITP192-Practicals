package Practical3;

public class Customer {
	public String name;
	public int custid;
	
	//Constructor
	public Customer () {
		
	}
	
	public Customer (String name, int custid) {
		this.name = name;
		this.custid = custid;
	}
	
	//Accessor and Mutator
	public String getName () {
		return name;
	}
	
	public int getid () {
		return custid;
	}
	
	public void setName (String name) {
		this.name = name;
	}
	
	public void setid (int id) {
		this.custid = id;
	}
	
	//result printer
	public void print () {
		
		System.out.println("Customer ID: " + custid);
		System.out.println("Customer Name: " + name);
	}
}
