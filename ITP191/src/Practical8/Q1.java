package Practical8;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Q1 {
	public static void main (String[] args) throws FileNotFoundException{
		File inputfile = new File("Text/hello.txt");
		Scanner in = new Scanner (inputfile);
		PrintWriter out = new PrintWriter("Text/hello.txt");
		
		String inputtext = "Hello World";
		
		out.print(inputtext);
		
		in.close();
		out.close();
		
		File inputfile2 =  new File("Text/hello.txt");
		Scanner in2 = new Scanner (inputfile2);
		String output = in2.nextLine();
		System.out.print(output);
	}
}
