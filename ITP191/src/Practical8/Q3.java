package Practical8;
import java.util.Scanner;

public class Q3 {
	public static void main(String[] args) {
			Scanner sc = new Scanner(System.in);
			
			try {
			System.out.print("Enter an integer numerator: ");
			int numerator = sc.nextInt();
			
			System.out.print("Enter an integer denominator: ");
			int denominator = sc.nextInt();
			
			
			int result = numerator / denominator; System.out.printf("Result of %d / %d is %d\n", numerator, denominator, result);
			} catch (Exception e){
				System.out.println("");
				System.out.println("ERROR!");
			}
			finally {
				System.out.println("Cleaning up resources..."); //Q4: Yes. This is show in all situation when there is an error.
				System.exit(0);
			}
	}
}
