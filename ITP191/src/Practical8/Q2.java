package Practical8;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Q2 {
	public static void main (String[] args) throws FileNotFoundException{
		File inputfile = new File("Text/marry.txt");
	
		Scanner in = new Scanner (inputfile);
		in.useDelimiter("/*");
		
		PrintWriter out = new PrintWriter("Text/marryedited.txt");
		
		int i = 0;
		while (in.hasNextLine()) {
			i++;
			String input = "/*" + i + "*/" + in.nextLine();
			System.out.println(input);
			out.println(input);
		}
		
		in.close();
		out.close();
	}
}
