package Practical8;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Q5 {
	public static void main(String[] args) throws FileNotFoundException{
		try {
			float total1 = 0;
			float total2 = 0;
			float average1 = 0;
			float average2 = 0;
			
			Scanner in = new Scanner (System.in);
			
			String filename;
			
			System.out.print("Enter name of file: ");
			filename = "Text/" + in.nextLine() + ".txt";
			
			File inputfile = new File (filename);
			Scanner input = new Scanner (inputfile);
			
			float[] numbers = new float[8];
			
			for (int i = 0 ; i < numbers.length ; i++) { //Store all numbers in array
				numbers[i] = input.nextFloat();
			}
			
			for (int j = 0 ; j < numbers.length ; j+=2) { //Total of column 1 (even)
				total1 = total1 + numbers[j];
			}
			
			for (int k = 0 ; k < numbers.length ; k++) { //Total of column 2 (odd)
				if (k%2 != 0) {
					total2 = total2 + numbers[k];
				}
			}
			
			average1 = total1 / 4;
			average2 = total2 / 4;
			
			System.out.println("Average of column 1: " + average1);
			System.out.println("Average of column 2: " + average2);
		} catch (Exception e) {
			System.out.print("ERROR! RESET SYSTEM");
			System.exit(0);
		}
	}
}
