package Practical4;
 
public class Message {
        private String sender;
        private String receiver;
        private String content;
        private String toString;
       
        public Message (String sender, String receiver) {
                this.sender = sender;
                this.receiver = receiver;
        }
       
        public String getSender () {
                return sender;
        }
       
        public String getReceiver () {
                return receiver;
        }
       
        public String getContent () {
                return content;
        }
       
        public void setSender (String sender) {
                this.sender = sender;
        }
       
        public void setReceiver (String receiver) {
                this.receiver = receiver;
        }
       
        public void setContent (String content) {
                this.content = content;
        }
       
        public String append () {
                content += " This is an appended line";
                return content;
               
        }
        
        public String toString() {
        	toString = "From: " + receiver + "\\n To: " + sender + "\\n Content: " + content;
            return toString;
    }
       
        public void view () {
                System.out.println("Sender: " + sender);
                System.out.println("Receiver: " + receiver);
                System.out.println("Content: " + content);
                System.out.println("String Content: " + toString);
        }
}