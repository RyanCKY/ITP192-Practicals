package Practical4;
import java.util.Scanner;
 
public class MessageApp {
        public static void main (String[] args) {
                Scanner in = new Scanner (System.in);
               
                String sender, receiver, content;
               
                System.out.print("Enter your email: ");
                sender = in.nextLine();
               
                System.out.print("Enter recipient email: ");
                receiver = in.nextLine();
               
                Message m1 = new Message(sender, receiver);
               
                System.out.print("Enter your message: ");
                content = in.nextLine();
                m1.setContent(content);
               
                System.out.println(" ");
               
                m1.toString();
                m1.append();
                m1.view();
                
                in.close();
        }
}