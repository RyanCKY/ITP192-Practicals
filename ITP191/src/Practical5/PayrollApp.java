package Practical5;
import java.util.ArrayList;
import java.util.Scanner;

public class PayrollApp {
	public static void main (String[] args) {
		Scanner in = new Scanner (System.in);
		
		ArrayList<Salaried> salarylist = new ArrayList<Salaried>();
		ArrayList<Manager> managerlist = new ArrayList<Manager>();
		ArrayList<Hourly> hourlylist = new ArrayList<Hourly>();
		
		Salaried s1;
		Manager m1;
		Hourly h1;
		
		String name, nric;
		int choice, displaychoice, days;
		double salary, bonus, rate;
		
		System.out.println("==========================");
		System.out.println("|       Main Menu        |");
		System.out.println("==========================");
		System.out.println("|     0. Calculate       |");
		System.out.println("|  1. Salaried Employee  |");
		System.out.println("|   2. Hourly Employee   |");
		System.out.println("|      3. Manager        |");
		System.out.println("==========================");
		
		do {
			System.out.print("Enter choice: ");
			choice = in.nextInt();
			
			if (choice == 1) {
				
				in.nextLine();
				
				System.out.print("Enter Name: ");
				name = in.nextLine();
				
				System.out.print("Enter NRIC: ");
				nric = in.nextLine();
				
				System.out.print("Enter Salary: $");
				salary = in.nextDouble();
				
				s1 = new Salaried();
				s1.setName(name);
				s1.setNric(nric);
				s1.setSalary(salary);
				s1.computeTotalMonthlySalary(salary);
				salarylist.add(s1);
				
				System.out.println(" ");
			}
			else if (choice == 2) {
				in.nextLine();
				
				System.out.print("Enter Name: ");
				name = in.nextLine();
				
				System.out.print("Enter NRIC: ");
				nric = in.nextLine();
				
				System.out.print("Enter Rate: $");
				rate = in.nextDouble();
				
				System.out.print("Enter Hours: ");
				int hour = in.nextInt();
				
				System.out.print("Enter worked days: ");
				days = in.nextInt();
				
				h1 = new Hourly();
				h1.setName(name);
				h1.setNric(nric);
				h1.setRate(rate);
				h1.setDays(days);
				h1.computeMonthlySalary(days, hour, rate);
				hourlylist.add(h1);
				
				System.out.println(" ");
			}
			else if (choice == 3) {
				in.nextLine();
				
				System.out.print("Enter Name: ");
				name = in.nextLine();
				
				System.out.print("Enter NRIC: ");
				nric = in.nextLine();
				
				System.out.print("Enter Salary: $");
				salary = in.nextDouble();
				
				System.out.print("Enter Bonus: $");
				bonus = in.nextDouble();
				
				m1 = new Manager();
				m1.setName(name);
				m1.setNric(nric);
				m1.setSalary(salary);
				m1.setBonus(bonus);
				m1.computeTotalSalary(salary, bonus);
				managerlist.add(m1);
				
				System.out.println(" ");
			}
			in.nextLine();
		} while (choice != 0);
		
		System.out.println("=============================================");
		System.out.println("|               Results Menu                |");
		System.out.println("=============================================");
		System.out.println("|     1. Display All Employee Details       |");
		System.out.println("|    2. Display Salaried Employee Details   |");
		System.out.println("|    3. Display Hourly Employee Details     |");
		System.out.println("|       4. Display Manager Details          |");
		System.out.println("|      5. Show Monthly Grand Total          |");
		System.out.println("=============================================");
		
		System.out.print("Enter output choice: ");
		displaychoice = in.nextInt();
		
		if (displaychoice == 1) {
			for (Salaried s2 : salarylist) {
				s2.viewBasic();
				s2.view();
				System.out.println(" ");
			}
			for (Hourly h2 : hourlylist) {
				h2.viewBasic();
				h2.view();
				System.out.println(" ");
			}
			for (Manager m2 : managerlist) {
				m2.viewBasic();
				m2.view();
				System.out.println(" ");
			}
			
		}
		else if (displaychoice == 2) {
			for (Salaried s2 : salarylist) {
				s2.viewBasic();
				s2.view();
				System.out.println(" ");
			}
		}
		else if (displaychoice == 3) {
			for (Hourly h2 : hourlylist) {
				h2.viewBasic();
				h2.view();
				System.out.println(" ");
			}
		}
		else if (displaychoice == 4) {
			for (Manager m2 : managerlist) {
				m2.viewBasic();
				m2.view();
				System.out.println(" ");
			}
		}
		else if (displaychoice == 5) {
			
		}
		in.close();
		System.out.print("~END OF PROGRAM~");
	}
}
