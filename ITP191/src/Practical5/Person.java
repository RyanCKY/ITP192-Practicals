package Practical5;

public class Person {
	private String name;
	private String nric;
	
	public Person (String nric, String name) {
		this.name = name;
		this.nric = nric;
	}
	
	public void setName (String name) {
		this.name = name;
	}
	
	public void setNric (String nric) {
		this.nric = nric;
	}
	
	public String getName () {
		return name;
	}
	
	public String getNric () {
		return nric;
	}
	
	public void viewBasic () {
		System.out.println("Name: " + name);
		System.out.println("NRIC: " + nric);
	}
}

//What are the compulsory attributes to create a student or a lecturer object?
//The compulsory attributes are the Name and NRIC.