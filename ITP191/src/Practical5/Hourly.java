package Practical5;
import java.util.ArrayList;

public class Hourly extends Employee {
	private int hours;
	private double rate;
	private double totalSalary;
	private double monthlySalary;
	private int days;
	protected ArrayList<Double> hourlyMonthlySalary = new ArrayList<Double>();
	
	public void setHourlySalary (double totalSalary) {
		totalSalary = hours * rate;
	}
	
	public double getHourlySalary () {
		return totalSalary;
	}
	
	public double computeMonthlySalary (int tDays, int tHour, double tRate) {
		monthlySalary = tDays * tHour * tRate;
		hourlyMonthlySalary.add(monthlySalary);
		return monthlySalary;
	}
	
	public void setRate (double rate) {
		this.rate = rate;
	}
	
	public void setDays (int days) {
		this.days = days;
	}
	
	public void view () {
		System.out.println("Rate: $" + rate);
		System.out.println("Number of Days worked: " + days);
		System.out.println("Total Monthly Salary: $" + monthlySalary);
	}
}
