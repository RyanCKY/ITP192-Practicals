package Practical5;
import java.util.ArrayList;
import java.util.Scanner;

public class PersonApp {
	public static void main (String[] args) {
		Scanner in = new Scanner (System.in);
		ArrayList<Student> studentlist = new ArrayList<Student>();
		ArrayList<Lecturer> lecturerlist = new ArrayList<Lecturer>();
		
		Student s1;
		Lecturer l1;
		
		int choice, displaychoice;
		String name = null, nric = null, staffId;
		double testMark, examMark, totalTeachingHours, salary = 0, finalMark = 0;
		
		System.out.println("===================");
		System.out.println("|    Main Menu    |");
		System.out.println("===================");
		System.out.println("|   0. Calculate  |");
		System.out.println("|    1. Student   |");
		System.out.println("|    2. Lecturer  |");
		System.out.println("===================");
		
		
		
		do {
			System.out.print("Enter type of entry: ");
			choice = in.nextInt();
			
			if (choice != 0) {
				if (choice == 1) {
					
					in.nextLine();
					
					System.out.print("Enter name of Student: ");
					name = in.nextLine();
					
					System.out.print("Enter NRIC: ");
					nric = in.nextLine();
					
					System.out.print("Enter Test Marks: ");
					testMark = in.nextDouble();
					
					System.out.print("Enter Exam Marks: ");
					examMark = in.nextDouble();
					
					s1 = new Student(nric, name);
					s1.setexamMark(examMark);
					s1.settestMark(testMark);
					finalMark = s1.computeFinalMark();
					studentlist.add(s1);
					
					System.out.println(" ");
				}
				else if (choice == 2) {
					
					in.nextLine();
					
					System.out.print("Enter name of Lecturer: ");
					name = in.nextLine();
					
					System.out.print("Enter NRIC: ");
					nric = in.nextLine();
					
					System.out.print("Enter Staff ID: ");
					staffId = in.nextLine();
					
					System.out.print("Enter Total Teaching Hours: ");
					totalTeachingHours = in.nextDouble();
					
					l1 = new Lecturer(name, nric);
					l1.setName(name);
					l1.setNric(nric);
					l1.setstaffId(staffId);
					l1.settotalTeachingHour(totalTeachingHours);
					salary = l1.computeSalary();
					lecturerlist.add(l1);
					
					System.out.println(" ");
				}
			}
			
			in.nextLine();
			
		} while (choice != 0);
		
		System.out.println("===================");
		System.out.println("|   Results Menu  |");
		System.out.println("===================");
		System.out.println("|     1. All      |");
		System.out.println("|    2. Student   |");
		System.out.println("|    3. Lecturer  |");
		System.out.println("===================");
		
		System.out.print("Enter output choice: ");
		displaychoice = in.nextInt();
		
		System.out.println(" ");
		
		if (displaychoice == 1) {
			for (Student s2 : studentlist) {
				s2.viewBasic();
				s2.view();
				System.out.println("Final Marks: " + finalMark);
				System.out.println(" ");
			}
			for (Lecturer l2 : lecturerlist) {
				l2.viewBasic();
				l2.view();
				System.out.println("Total Salary: $" + salary);
				System.out.println(" ");
			}
			
		}
		else if (displaychoice == 2) {
			for (Student s2 : studentlist) {
				s2.viewBasic();
				s2.view();
				System.out.println("Final Marks: " + finalMark);
				System.out.println(" ");
			}
		}
		else if (displaychoice == 3) {
			for (Lecturer l2 : lecturerlist) {
				l2.viewBasic();
				l2.view();
				System.out.println("Total Salary: $" + salary);
				System.out.println(" ");
			}
		}
		
		in.close();
		System.out.print("~END OF PROGRAM~");
	}
}
