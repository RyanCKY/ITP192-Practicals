package Practical5;
import java.util.ArrayList;

public class Salaried extends Employee {
	private double salary;
	private double totalsalary;
	protected ArrayList<Double> salariedMonthlySalary = new ArrayList<Double>();
	
	
	public void setSalary (double salary) {
		this.salary = salary;
	}
	
	public double getSalary () {
		return salary;
	}
	
	public double computeTotalMonthlySalary (double salary) {
		totalsalary = salary;
		salariedMonthlySalary.add(totalsalary);
		return totalsalary;
	}
	
	public void view () {
		System.out.println("Salary: $" + salary);
		System.out.println("Total Monthly Salary: $" + totalsalary);
	}
}
