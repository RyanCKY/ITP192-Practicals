package Practical5;

public class Employee {
	private String name;
	private String nric;
	
	public void setName (String name) {
		this.name = name;
	}
	
	public void setNric (String nric) {
		this.nric = nric;
	}
	
	public String getName () {
		return name;
	}
	
	public String getNric () {
		return nric;
	}
	
	public void viewBasic () {
		System.out.println("Name: " + name);
		System.out.println("NRIC: " + nric);
	}
}
