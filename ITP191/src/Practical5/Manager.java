package Practical5;
import java.util.ArrayList;

public class Manager extends Employee {
	private double salary;
	private double totalsalary;
	private double bonus;
	protected ArrayList<Double> managerMonthlySalary = new ArrayList<Double>();
	
	public void setSalary (double salary) {
		this.salary = salary;
	}
	
	public void setBonus (double bonus) {
		this.bonus = bonus;
	}
	
	public double getSalary () {
		return salary;
	}
	
	public double getBonus () {
		return bonus;
	}
	
	public double computeTotalSalary (double salary, double bonus) {
		totalsalary = salary + bonus;
		managerMonthlySalary.add(totalsalary);
		return totalsalary;
	}
	
	public void view () {
		System.out.println("Salary: $" + salary);
		System.out.println("Bonus: $" + bonus);
		System.out.println("Total Monthly Salary: $" + totalsalary);
	}
}
