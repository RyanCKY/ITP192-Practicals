package Practical5;

public class Student extends Person{
	private double testMark;
	private double examMark;
	private double finalMark;
	
	public Student(String nric, String name) {
		super(nric, name);
		this.testMark = testMark;
		this.examMark = examMark;
	}
	
	public void settestMark (double testMark) {
		this.testMark = testMark;
	}
	
	public void setexamMark (double examMark) {
		this.examMark = examMark;
	}
	
	public double gettestMark () {
		return testMark;
	}
	
	public double getexamMark () {
		return examMark;
	}
	
	public double computeFinalMark () {
		finalMark = (0.5 * examMark) + (0.5 * testMark);
		return finalMark;
	}
	
	public void view () {
		System.out.println("Test Mark: " + testMark);
		System.out.println("Exam Mark: " + examMark);
	}
}
