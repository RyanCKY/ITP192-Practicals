package Practical5;

public class Lecturer extends Person{
	private String staffId;
	private double totalTeachingHour;
	private double totalSalary;
	
	public Lecturer(String nric, String name) {
		super(nric, name);
		this.staffId = staffId;
		this.totalTeachingHour = totalTeachingHour;
	}
	
	public void setstaffId (String staffId) {
		this.staffId = staffId;
	}
	
	public void settotalTeachingHour (double totalTeachingHour) {
		this.totalTeachingHour = totalTeachingHour;
	}
	
	public String getstaffId () {
		return staffId;
	}
	
	public double gettotalTeachingHour () {
		return totalTeachingHour;
	}
	
	public double computeSalary () {
		totalSalary = totalTeachingHour * 90;
		return totalSalary;
	}
	
	public void view () {
		System.out.println("Staff ID: " + staffId);
		System.out.println("Total Teaching Hours: " + totalTeachingHour);
	}
}
